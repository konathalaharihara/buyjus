import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import { RestClientService } from './rest-client.service';

describe('RestClientService', () => {
  // beforeEach(() => TestBed.configureTestingModule({}));
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule], 
    providers: [RestClientService]
  }));


  it('should be created', () => {
    const service: RestClientService = TestBed.get(RestClientService);
    expect(service).toBeTruthy();
  });
  it('should have getData function', () => {
    const service: RestClientService = TestBed.get(RestClientService);
    expect(service.get).toBeTruthy();
   });
});
