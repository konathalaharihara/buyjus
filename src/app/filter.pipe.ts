import { Pipe, PipeTransform } from '@angular/core';
import { ConcatSource } from 'webpack-sources';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {
    constructor() { }
    transform(value: any[], searchString: string ) {
        if (!searchString){
          return value;
        }
        return value.filter(res => {
            const title = res.title.toLowerCase().toString().includes(searchString.toLowerCase());
            const experience = res.experience.toLowerCase().includes(searchString.toLowerCase());
            const skills = res.skills.toLowerCase().includes(searchString.toLowerCase());
            const companyname = res.companyname.toLowerCase().includes(searchString.toLowerCase());
            const location = res.location.toLowerCase().includes(searchString.toLowerCase());
            return (title + experience + skills + location + companyname );
        });
     }
}