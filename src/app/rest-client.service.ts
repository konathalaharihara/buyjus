import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  constructor(private httpClient: HttpClient) {}
  get(url: string) {
      let headers = new HttpHeaders();
      headers = headers.set('Accept', 'application/json');
      return this.httpClient.get(url, {
          headers: headers,
      });
  }
  
}
