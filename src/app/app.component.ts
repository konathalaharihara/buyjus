import { Component, OnInit } from '@angular/core';
import { RestClientService } from './rest-client.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'buyjus-task';
  info = [];
  location: any;
  experiences: any;
  skills: any;
  page: number;
  number: number;
  start: number;
  end: number;
  currentPage: number;
  pagestart: number;
  pageend: number;
  datainfo = [];
  searchtext: any;
  order: any;
  constructor(private http: RestClientService) {
  }
  ngOnInit() {
    this.page = 4;
    this.start = 0;
    this.end = 10;
    this.currentPage = 0
    this.pagestart = 0;
    this.pageend = 10;
    this.searchtext = '';
    this.order = '';
    this.getEndpointInfo()
  }
  getEndpointInfo() {
    this.http.get('https://nut-case.s3.amazonaws.com/jobs.json').subscribe(res => {
      if (res !== null) {
        this.location = Array.from(new Set(res['data'].map(result => result.location)));
        this.experiences = Array.from(new Set(res['data'].map(result => result.experience)));
        this.skills = Array.from(new Set(res['data'].map(result => result.skills)));
        this.datainfo = res['data'];
        this.info = this.datainfo.filter(res => res.experience === 'Freshers'|| res.experience === 'Fresher' ? res.experience = '0 yrs' : res.experience);
      }
    })
  }
  getpages() {
    const loop = [];
    for (let i = 1; i <= Math.ceil(this.datainfo.length / 10); i++) {
      loop.push(i);
    }
    return loop;
  }
  pagination(value) {
    if (value === 'Previous') {
      if (this.pagestart === 0) {
        this.currentPage--;
        this.start = 10 * this.currentPage;
        this.end = 10 * this.currentPage + 10;
      } else {
        --this.pagestart;
        --this.pageend;
        this.currentPage--;
        this.start = 10 * this.currentPage;
        this.end = 10 * this.currentPage + 10;
      }
    } else if (value === 'Next') {
      ++this.pagestart;
      ++this.pageend;
      this.currentPage++;
      this.start = 10 * this.currentPage + 1;
      this.end = 10 * this.currentPage + 1 + 10;
    } else {
      this.currentPage = value - 1;
      this.start = 10 * this.currentPage;
      this.end = 10 * this.currentPage + 10;
    }
  }
  filter(value, string) {
    this.searchtext = value;
    this.info = this.datainfo.filter(res => res[string] == value);
  }
  sort(value) {
    this.order = value;
  }
  shortOrder(value) {
    if (this.order !== '') {
      if (value === 'asc') {
        this.info = this.datainfo.sort((a, b) => a[this.order].localeCompare(b[this.order]));
      } else {
        this.info = this.datainfo.sort((a, b) => b[this.order].localeCompare(a[this.order]));
      }
    }
  }
}
